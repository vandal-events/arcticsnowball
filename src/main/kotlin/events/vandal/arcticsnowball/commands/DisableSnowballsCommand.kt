package events.vandal.arcticsnowball.commands

import events.vandal.arcticsnowball.ArcticSnowball
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

object DisableSnowballsCommand : CommandExecutor {
    override fun onCommand(
        sender: CommandSender,
        command: Command,
        string: String,
        args: Array<out String>
    ): Boolean {
        ArcticSnowball.snowballsEnabled = !ArcticSnowball.snowballsEnabled

        if (ArcticSnowball.snowballsEnabled) {
            sender.sendMessage("${ChatColor.GREEN}>>> Enabled snowballs.")
        } else {
            sender.sendMessage("${ChatColor.YELLOW}>>> Disabled snowballs.")
        }

        return true
    }
}